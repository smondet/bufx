Bufx: Passive Buffered Transforms
=================================


This mini-library provides (packed in `Bufx`) the module `Transform`
which implements the API defined in `TRANSFORM`.

`Bufx.Transform.t` is a container for buffered transformations over
streams modeled in the style of the [Cryptokit] library for the
[Biocaml] project (see the `biocaml` application for extensive use
—and testing— of the module).

Being *“Passive”* means that a transform does not *pull* from a
stream, it has to be *fed* with data; this allows to write stream
transformations (like file-formats parsing/printing) that are
compatible with both “normal” I/O (like `in_channel`) and asynchronous
libraries (like `Lwt_unix` or `Async_unix`).

[Cryptokit]: http://forge.ocamlcore.org/projects/cryptokit/
[Biocaml]: http://biocaml.org


Composition Functions
---------------------

The `TRANSFORM` API provides functions to play *Lego* with basic
`Transform.t` values in order to build more complex ones. Here are
graphical representations of those functions:

<pre>val compose: ('a, 'b) t -> ('b, 'c) t -> ('a, 'c) t</pre>
<div class="figure" style="max-width : 50%" title="Compose">
    <img src="doc/figures/transform_compose.svg" width="99%"/>
</div>

<pre>
val mix : ('a1, 'b1) t -> ('a2, 'b2) t ->
  ('a1 * 'a2, [ `both of 'b1 * 'b2 | `left of 'b1 | `right of 'b2 ]) t
</pre>
<div class="figure" style="max-width : 50%" title="Mix">
    <img src="doc/figures/transform_mix.svg" width="99%"/>
</div>

<pre>
val filter_compose: ('il, 'ol) t -> ('ir, 'our) t ->
    destruct:('ol -> [`transform of 'ir | `bypass of 'filtered]) ->
    reconstruct:([`bypassed of 'filtered | `transformed of 'our] -> 'result) ->
    ('il, 'result) t
</pre>
<div class="figure" style="max-width : 50%" title="Filter-Compose">
    <img src="doc/figures/transform_filter_compose.svg" width="99%"/>
</div>

<pre>
val split_and_merge:
    ('il, 'ol) t -> ('ir, 'our) t ->
    split:('input -> [`left of 'il | `right of 'ir]) ->
    merge:([`left of 'ol | `right of 'our] -> 'output) ->
    ('input, 'output) t
</pre>
<div class="figure" style="max-width : 50%" title="Split and Merge">
    <img src="doc/figures/transform_split_merge.svg" width="99%"/>
</div>

<pre>
val compose_results:
    on_error:([`left of 'error_left | `right of 'error_right ] -> 'error) ->
    ( 'input_left, ('middle, 'error_left) result) t ->
    ( 'middle, ('output_right, 'error_right) result) t ->
    ( 'input_left, ('output_right, 'error) result) t
</pre>
<div class="figure" style="max-width : 50%" title="Compose results">
    <img src="doc/figures/transform_compose_results.svg" width="99%"/>
</div>

<pre>
val compose_result_left:
    ('input_left, ('middle, 'error) result) t ->
    ('middle, 'output_right) t ->
    ('input_left, ('output_right, 'error) result) t
</pre>
<div class="figure" style="max-width : 50%" title="Compose results on the left">
    <img src="doc/figures/transform_compose_result_left.svg" width="99%"/>
</div>
